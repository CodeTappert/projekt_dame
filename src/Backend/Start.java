package Backend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class Start extends Application {
    static Stage primaryStage;

    public static void main(String[] args) {
        Application.launch(args);
    }

    public void start(Stage Stage) {
        primaryStage = Stage;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("../Frontend/Spielfeld.fxml"));
        try {
            AnchorPane root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setMinHeight(1600);
            primaryStage.setMinWidth(900);
            primaryStage.setResizable(false);
            primaryStage.setTitle("Toolkit");
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }}

