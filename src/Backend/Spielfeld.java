package Backend;

/**
 * Klasse die sich mit der Erstellung und Verwaltung des Spielfelds befasst..
 */
public class Spielfeld {
    /**
     * Das logische Spielfeld
     */
    private static byte[][] feld = new byte[8][8];


    /**
     * Simple Getter Methode für eine bestimmte Zelle des Arrays
     *
     * @param X X-Koordinate
     * @param Y Y-Koordinate
     * @return Wert in dem durch X und Y angegeben Feld.
     */
    public static byte getFeldValue(byte X, byte Y) {
        try {
            return feld[X][Y];
        } catch (ArrayIndexOutOfBoundsException e) {
            return -1;
        }

    }

    /**
     * Simple Setter Methode für eine bestimmte Zelle des Arrays
     *
     * @param X     X-Koordinate
     * @param Y     Y-Koordinate
     * @param Value Wert der in die Zelle geschrieben wird.
     */
    public static void setFeldValue(byte X, byte Y, byte Value) {
        feld[X][Y] = Value;
    }

    /**
     * Methode zum intialisieren des Feldes mit entsprechend gesetzten Steinen.
     * 0=Leer,1=WeißNormal,2=SchwarzNormal,3=Weiß Dame,4 = Schwarz Dame;
     */
    public static void StartFeld() {
        for (byte i = 0; i < feld.length; i++) {
            for (byte j = 0; j < 8; j++) {
                if (((i == 0 || i == 2) && (j == 0 || j == 2 || j == 4 || j == 6)) || (i == 1 && (j == 1 || j == 3 || j == 5 || j == 7))) {
                    feld[j][i] = 1;
                } else if (((i == 5 || i == 7) && (j == 1 || j == 3 || j == 5 || j == 7)) || (i == 6 && (j == 0 || j == 2 || j == 4 || j == 6))) {
                    feld[j][i] = 2;
                } else {
                    feld[j][i] = 0;

                }
            }
        }
    }
}

