package Backend;

import Frontend.SpielfeldController;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import static Frontend.SpielfeldController.GetArt;

public class Spielablauf {
    static Spielablauf Ablauf = new Spielablauf();
    static Zug Zuge = new Zug();
    static SpielfeldController Controller = new SpielfeldController();


    byte Spieler, SteinArt, XAlt, XNeu, YAlt, YNeu;
    public int SpielerRightNow;
    public int FigurArt;

    public void SpielerWechseln(int Farbennummer) {
        SpielerRightNow = Farbennummer;
        System.out.println("SPieler   " + SpielerRightNow);
    }

    public void StarteSpiel() {
        SpielerWechseln(1);
        Spielfeld.StartFeld();
    }

    public void ZugAusfuehren() {
        if (GetArt() == 3 || GetArt() == 4) {
            FigurArt = 2;
        } else if (GetArt() == 2 || GetArt() == 1) {
            FigurArt = 1;
        }

        Zuge.Zug((byte) SpielerRightNow, SpielfeldController.GetXAlt(), SpielfeldController.GetXNeu(), SpielfeldController.GetYAlt(), SpielfeldController.GetYNeu(), (byte) FigurArt);
        System.out.println("Werteeeee: " + Zuge.ZugErfolgreich);
        if (Zuge.ZugErfolgreich == 1) {
            if (SpielerRightNow == 1) {
                SpielerWechseln(2);
                System.out.println("Schwarz ist dran");
            } else {
                SpielerWechseln(1);
                System.out.println("Weiß ist dran");
            }
            Zuge.ErfolgreicherZugZuende();
        }
        Controller.F20.setImage(null);
        System.out.println("Ausgeführt");

    }


}


