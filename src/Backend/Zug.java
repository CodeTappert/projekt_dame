package Backend;

import static Backend.Spielfeld.getFeldValue;
import static Backend.Spielfeld.setFeldValue;

/**
 * Klasse die sich mit dem Ziehen eines Steines beschäftigt.
 */
public class Zug {

    Spielablauf Ablauf = new Spielablauf();
    int ZugErfolgreich = 0;

    /**
     * Methode die den Zug eines Steins realisiert
     *
     * @param Spieler Welcher Spieler. 1 = weiß, 2 = schwarz.
     * @param XAlt    Ausgangsposition X-Wert.
     * @param XNeu    X-Wert (Des zu schlagenden Steines falls vorhanden) sonst Zielfeld des Zuges
     * @param YAlt    Ausgangsposition Y-Wert.
     * @param YNeu    Y-Wert (Des zu schlagenden Steins falls vorhanden) sonst Zieldfeld des Zuges
     * @param Art     Art der Figur. 1=normaler Stein, 2=Dame.
     */
    public void Zug(byte Spieler, byte XAlt, byte XNeu, byte YAlt, byte YNeu, byte Art) {
        boolean S_Movable = false;
        boolean W_Movable = false;
        if (Spieler == 1) {
            W_Movable = true;
        } else if (Spieler == 2) {
            S_Movable = true;
        }
        System.out.println(S_Movable + "    " + W_Movable);
        boolean bewegbar = CanMoveThisStone(W_Movable, S_Movable, XAlt, YAlt);
        boolean feldfrei = MovableOnFeld(Spieler, XAlt, XNeu, YAlt, YNeu, Art);
        if (bewegbar && feldfrei) {
            if (Spieler == 1) {
                if (Art == 1) {
                    if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 1) || XNeu == XAlt - 1)) && (YNeu == YAlt + 1)) {
                        setFeldValue(XAlt, YAlt, (byte) 0);
                        setFeldValue(XNeu, YNeu, (byte) 1);
                        ZugErfolgreich = 1;
                    } else if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 2) || XNeu == XAlt - 2)) && (YNeu == YAlt + 2) &&
                            (((Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 1)) || (
                                    (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 1)))) {
                        setFeldValue(XAlt, YAlt, (byte) 0);
                        setFeldValue(XNeu, YNeu, (byte) 1);
                        System.out.println("2");
                        if ((Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 1)) {
                            setFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1), (byte) 0);
                            ZugErfolgreich = 1;
                            System.out.println("3");
                        } else if ((Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 1)) {
                            setFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1), (byte) 0);
                            ZugErfolgreich = 1;
                            System.out.println("4");
                        }
                    } else {
                        System.out.println("Fehler" + 1);
                    }
                } else if (Art == 2) {
                    if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && ((YNeu - YAlt) == (XNeu - XAlt))) {

                    }
                } else {
                    System.out.println("Fehler " + 2);
                }
            } else if (Spieler == 2) {
                if (Art == 1) {
                    if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 1) || XNeu == XAlt - 1)) && (YNeu == YAlt - 1)) {

                    } else if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 2) || XNeu == XAlt - 2)) && (YNeu == YAlt - 2) &&
                            ((Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt - 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt - 1)) == 1) ||
                                    (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt - 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt - 1)) == 1))) {

                    } else {
                        System.out.println("Fehler " + 3);

                    }
                } else if (Art == 2) {
                    if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && ((YNeu - YAlt) == (XNeu - XAlt))) {

                    }
                } else {
                    System.out.println("Fehler " + 4);

                }
            }
        }
    }


    /**
     * @param weiss   Sind weiße Steine von diesem Spieler bewegbar?
     * @param schwarz Sind schwarze Steine von diesem Spieler bewegbar?
     * @param X       X-Koordinate des Feldes
     * @param Y       Y-Koordinate des Feldes
     * @return true = darf Stein bewegen, false = darf stein nicht bewegen.
     */
    public static boolean CanMoveThisStone(boolean weiss, boolean schwarz, byte X, byte Y) {
        System.out.println("X ist:" + X + " Y ist:" + Y);
        byte inhalt = getFeldValue(X, Y);
        System.out.println(inhalt + "ist der Inhalt");
        if ((inhalt == 1 || inhalt == 2) && (weiss == true || schwarz == true)) {
            System.out.println("Stein kann bewegt werden");
            return true;
        } else
            System.out.println("1. Das soll nicht passieren");
        return false;
    }

    /**
     * Funktion welche prüft ob auf das ausgewählte Feld überhaupt gezogen werden darf, und wenn dort eine Figur steht das dahinterstehende Feld prüft.
     *
     * @param Spieler Welcher Spieler will ziehen? 1=weiß,2=schwarz.
     * @param XAlt    Ausgangsposition X-Wert.
     * @param XNeu    X-Wert (Des zu schlagenden Steines falls vorhanden) sonst Zielfeld des Zuges
     * @param YAlt    Ausgangsposition Y-Wert.
     * @param YNeu    Y-Wert (Des zu schlagenden Steins falls vorhanden) sonst Zieldfeld des Zuges
     * @param Art     Art der Figur. 1=normaler Stein, 2=Dame.
     * @return true falls auf das Feld gezogen werden bzw. geschlagen werden kann., false falls nicht.
     * TODO Zug für Dame.
     */
    public boolean MovableOnFeld(byte Spieler, byte XAlt, byte XNeu, byte YAlt, byte YNeu, byte Art) {
        System.out.println("X  " + XNeu + "Y  " + YNeu);
        System.out.println(Spielfeld.getFeldValue(XNeu, YNeu));
        if (Spieler == 1) {
            if (Art == 1) {
                if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 1) || XNeu == XAlt - 1)) && (YNeu == YAlt + 1)) {
                    return true;
                } else if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 2) || XNeu == XAlt - 2)) && (YNeu == YAlt + 2) &&
                        ((Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt + 1)) == 1) ||
                                (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt + 1)) == 1))) {
                    return true;
                } else {
                    System.out.println("Fehler " + 1);
                    return false;
                }
            } else if (Art == 2) {
                if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && ((YNeu - YAlt) == (XNeu - XAlt))) {
                    return true;
                }
            } else {
                System.out.println("Fehler " + 2);
                return false;
            }
        } else if (Spieler == 2) {
            if (Art == 1) {
                if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 1) || XNeu == XAlt - 1)) && (YNeu == YAlt - 1)) {
                    return true;
                } else if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && (((XNeu == XAlt + 2) || XNeu == XAlt - 2)) && (YNeu == YAlt - 2) &&
                        ((Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt - 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt + 1), (byte) (YAlt - 1)) == 1) ||
                                (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt - 1)) == 2) || (Spielfeld.getFeldValue((byte) (XAlt - 1), (byte) (YAlt - 1)) == 1))) {
                    return true;
                } else {
                    System.out.println("Fehler " + 3);
                    return false;
                }
            } else if (Art == 2) {
                if ((Spielfeld.getFeldValue(XNeu, YNeu) == 0) && ((YNeu - YAlt) == (XNeu - XAlt))) {
                    return true;
                }
            } else {
                System.out.println("Fehler " + 4);
                return false;
            }
        }
        return true;
    }

    public void ErfolgreicherZugZuende() {
        ZugErfolgreich = 0;
    }
}



