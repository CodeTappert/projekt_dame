package Frontend;

import Backend.Spielablauf;
import Backend.Spielfeld;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.embed.swing.SwingFXUtils;

public class SpielfeldController implements Initializable {

    @FXML
   public ImageView F00, F01, F02, F03, F04, F05, F06, F07, F10, F11, F12, F13, F14, F15, F16, F17, F20, F21, F22, F23, F24, F25, F26, F27, F30, F31, F32, F33, F34, F35, F36, F37, F40, F41, F42, F43, F44, F45, F46, F47, F50, F51, F52, F53, F54, F55, F56, F57, F60, F61, F62, F63, F64, F65, F66, F67, F70, F71, F72, F73, F74, F75, F76, F77;


    @FXML
    GridPane GridImageView, GridBlackWhite;

    ImageView ImageViewClickOne = new ImageView();
    ImageView ImageViewClickTwo = new ImageView();
    double XKoordinate;
    double XID;
    double YKoordinate;
    double YID;
    String[][] FeldArray = new String[8][8];
    int i = 1;
    static int XAlt, XNeu, YAlt, YNeu;
    ImageView ImageViewToTest = new ImageView();
    String TestString = "TEST";
    byte Farbe = 0;
    static byte Art = 0;
    BufferedImage buffimageBN;
    BufferedImage buffimageBD;
    BufferedImage buffimageWD;
    BufferedImage buffimageWN;

    {
        try {
            buffimageBN = ImageIO.read(new File(getClass().getResource("/Images/BlackNormal.png").toURI()));
            buffimageBD = ImageIO.read(new File(getClass().getResource("/Images/BlackDame.png").toURI()));
            buffimageWD = ImageIO.read(new File(getClass().getResource("/Images/WhiteDame.png").toURI()));
            buffimageWN = ImageIO.read(new File(getClass().getResource("/Images/WhiteNormal.png").toURI()));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Image BN = SwingFXUtils.toFXImage(buffimageBN, null);
    public Image BD = SwingFXUtils.toFXImage(buffimageBD, null);
    public Image WD = SwingFXUtils.toFXImage(buffimageWD, null);
    public Image WN = SwingFXUtils.toFXImage(buffimageWN, null);

    static Spielablauf Ablauf = new Spielablauf();


    /**
     * Funktion um die ID des ImageView zu bekommen die vom Mouse Event gedrückt wurde.
     */
    @FXML
    public void getImageViewID(MouseEvent event) {

        XKoordinate = event.getX();
        YKoordinate = event.getY();
        CalcField();
    }


    public void CalcField() {
        XID = XKoordinate / 80;
        YID = YKoordinate / 80;
        //ABSICHT!
        int intY = (int) XID;
        int intX = (int) YID;
        String Koordinaten = "F" + intX + intY;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                TestString = "F" + i + j;
                try {
                    ImageViewToTest.setId(TestString);
                    CheckIfEqualsImageView(ImageViewToTest, Koordinaten);
                } catch (Exception e) {
                    System.out.println("FEHLER");
                }
            }
        }

        if (i == 2) {
            XAlt = intX;
            YAlt = intY;
            System.out.println("FirstClick");
        } else if (i == 1) {
            XNeu = intX;
            YNeu = intY;
            System.out.println("SecondClick");
            Ablauf.ZugAusfuehren();
        }
        System.out.println("IDS:" + ImageViewClickOne.getId() + "   " + ImageViewClickTwo.getId());
    }

    public static byte GetArt() {
        Art = Spielfeld.getFeldValue((byte) XAlt, (byte) YAlt);
        System.out.println("Art" + Art);
        return Art;

    }

    public void CheckIfEqualsImageView(ImageView FXX, String Koordinaten) {
        if (FXX.getId().equals(Koordinaten)) {
            if (i == 1) {
                ImageViewClickOne.setId(Koordinaten);
                i = 2;
            } else if (i == 2) {
                ImageViewClickTwo.setId(Koordinaten);
                i = 1;
            }
        }
    }


    public void SetImage(Image image, ImageView FXX) {
        FXX.setImage(image);
    }

    public static byte GetXAlt() {
        return (byte) XAlt;
    }

    public static byte GetXNeu() {
        return (byte) XNeu;
    }

    public static byte GetYAlt() {
        return (byte) YAlt;
    }

    public static byte GetYNeu() {
        return (byte) YNeu;
    }

    public ImageView GetFirstClick() {
        return ImageViewClickOne;
    }

    public ImageView GetSecondClick() {
        return ImageViewClickTwo;
    }


    public void setUpArray() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                FeldArray[i][j] = "F" + i + j;
            }
        }
    }

    public void setFigurPictures() {
        SetImage(WN, F00);
        SetImage(WN, F20);
        SetImage(WN, F40);
        SetImage(WN, F60);
        SetImage(WN, F11);
        SetImage(WN, F31);
        SetImage(WN, F51);
        SetImage(WN, F71);
        SetImage(WN, F02);
        SetImage(WN, F22);
        SetImage(WN, F42);
        SetImage(WN, F62);
        SetImage(BN, F15);
        SetImage(BN, F35);
        SetImage(BN, F55);
        SetImage(BN, F75);
        SetImage(BN, F06);
        SetImage(BN, F26);
        SetImage(BN, F46);
        SetImage(BN, F66);
        SetImage(BN, F17);
        SetImage(BN, F37);
        SetImage(BN, F57);
        SetImage(BN, F77);
        SetImage(WN, F31);


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Ablauf.StarteSpiel();
        setFigurPictures();
        setUpArray();


    }
}

